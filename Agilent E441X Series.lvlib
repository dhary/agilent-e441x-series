﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">This driver configures and takes measurements from theAgilent E441X Series.  For additional information about this driver, please refer to the Agilent E441X Series Readme.html</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*(!!!*Q(C=\&gt;1R&lt;NN!%)8BZS"&amp;UE5X#&amp;3FHSPQ#LT#&gt;+H6OG2L)"9Q21YA85&amp;8G.QAOI+OM0GZ(.I'%EB.$#3!O6[&lt;?LO=`&lt;3E+@8DA`2?\&gt;;RPXLU]69H^08DPP7WR-NJ(WBL?TH?VK1NZ]N&amp;TW6L[F/2@@ON`LL_H];P_X8V?"N`'`_`RW]=T,G`/?G6$_GO3_F*$\L4L8_\FO2*HO2*HO2*(O2"(O2"(O2"\O2/\O2/\O2/&lt;O2'&lt;O2'&lt;O2'XBOZS%5O=H9F74R:+#G;&amp;%AG1V(S+`%EHM34?0CIR*.Y%E`C34R-5?**0)EH]31?,F0C34S**`%E(EJV3@:'DC@R5&amp;["*`!%HM!4?&amp;B3A3=!")M&amp;B9-C-"1-"C?"*`!%(EY6?!*0Y!E]A9&gt;B":\!%XA#4_$BELYLU46T)]&gt;$'4E?R_.Y()`DI&lt;1=D_.R0)\(]&lt;#=()`D=2$/AEZR#()O=C9Y(RS0Y_'0()`D=4S/R`%QV/_1^ZW:.8-DRW.Y$)`B-4S'BR)S0)&lt;(]"A?QU.:'2\$9XA-D_&amp;B+2E?QW.Y$)CR+-P,+':=;%QS!M0$4X^;L._F[",LL@YVFQ&gt;6^1#K(CT6![.[%&amp;1X7(8D6$&gt;%N&gt;'K$62ND/I,K\[)#KB;7&amp;61.6%8@J`J*`K20N&amp;X^*%_U,@UT8TJ8ZZYO6RU0J^V/JVU0"YV4:.WOZX'=&gt;1Q$.JON^JM.E^PK]_UZ&lt;B&lt;XUPXH%]`0BU?0Y[(\V^`0DR_'1\@=DD-_7PWZ&lt;XU,\Q&lt;^5\NR7O?0@I&amp;JI?321!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">2.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>

		<Item Name="Action-Status" Type="Folder">
			<Item Name="Calculate" Type="Folder">
				<Item Name="Clear Fail Count.vi" Type="VI" URL="../Public/Action-Status/Calculate/Clear Fail Count.vi"/>
				<Item Name="Get Limits Failure.vi" Type="VI" URL="../Public/Action-Status/Calculate/Get Limits Failure.vi"/>
			</Item>
			<Item Name="Calibration" Type="Folder">
				<Item Name="Calibration.vi" Type="VI" URL="../Public/Action-Status/Calibration/Calibration.vi"/>

			</Item>
			<Item Name="E Series Sensors" Type="Folder">
				<Item Name="E9320 Clear Peak Hold.vi" Type="VI" URL="../Public/Action-Status/E Series Sensors/E9320 Clear Peak Hold.vi"/>

				<Item Name="E9320 Normal Path Zeroing.vi" Type="VI" URL="../Public/Action-Status/E Series Sensors/E9320 Normal Path Zeroing.vi"/>
			</Item>
			<Item Name="Memory" Type="Folder">
				<Item Name="Available Memory.vi" Type="VI" URL="../Public/Action-Status/Memory/Available Memory.vi"/>
			</Item>
			<Item Name="Status" Type="Folder">
				<Item Name="Get Status Registers.vi" Type="VI" URL="../Public/Action-Status/Status/Get Status Registers.vi"/>

				<Item Name="Set Status Registers.vi" Type="VI" URL="../Public/Action-Status/Status/Set Status Registers.vi"/>
			</Item>
			<Item Name="Trigger" Type="Folder">
				<Item Name="Abort.vi" Type="VI" URL="../Public/Action-Status/Trigger/Abort.vi"/>
				<Item Name="Initiate.vi" Type="VI" URL="../Public/Action-Status/Trigger/Initiate.vi"/>
				<Item Name="Trigger Immediate.vi" Type="VI" URL="../Public/Action-Status/Trigger/Trigger Immediate.vi"/>
			</Item>
			<Item Name="Enable LXI Identification.vi" Type="VI" URL="../Public/Action-Status/Enable LXI Identification.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Calculate" Type="Folder">
				<Item Name="Configure Gain.vi" Type="VI" URL="../Public/Configure/Calculate/Configure Gain.vi"/>
				<Item Name="Configure Limits.vi" Type="VI" URL="../Public/Configure/Calculate/Configure Limits.vi"/>
				<Item Name="Configure Math.vi" Type="VI" URL="../Public/Configure/Calculate/Configure Math.vi"/>
				<Item Name="Configure Frequency Sweep.vi" Type="VI" URL="../Public/Configure/Calculate/Configure Frequency Sweep.vi"/>

				<Item Name="Feed.vi" Type="VI" URL="../Public/Configure/Calculate/Feed.vi"/>
				<Item Name="Get Math Expressions.vi" Type="VI" URL="../Public/Configure/Calculate/Get Math Expressions.vi"/>
				<Item Name="Reset Ref Value.vi" Type="VI" URL="../Public/Configure/Calculate/Reset Ref Value.vi"/>
				<Item Name="Set Ref Value State.vi" Type="VI" URL="../Public/Configure/Calculate/Set Ref Value State.vi"/>
			</Item>
			<Item Name="Calibration" Type="Folder">
				<Item Name="Calibration Lockout.vi" Type="VI" URL="../Public/Configure/Calibration/Calibration Lockout.vi"/>
				<Item Name="Set TTL Calibration State.vi" Type="VI" URL="../Public/Configure/Calibration/Set TTL Calibration State.vi"/>
			</Item>
			<Item Name="E Series Sensors" Type="Folder">
				<Item Name="E Series Configure Input.vi" Type="VI" URL="../Public/Configure/E Series Sensors/E Series Configure Input.vi"/>
				<Item Name="E Series Configure Measurement.vi" Type="VI" URL="../Public/Configure/E Series Sensors/E Series Configure Measurement.vi"/>
				<Item Name="E Series Offset Table.vi" Type="VI" URL="../Public/Configure/E Series Sensors/E Series Offset Table.vi"/>
				<Item Name="E9320 Configure Gate.vi" Type="VI" URL="../Public/Configure/E Series Sensors/E9320 Configure Gate.vi"/>
				<Item Name="E9320 Configure Trace.vi" Type="VI" URL="../Public/Configure/E Series Sensors/E9320 Configure Trace.vi"/>

				<Item Name="E9320 Sensor Parameters.vi" Type="VI" URL="../Public/Configure/E Series Sensors/E9320 Sensor Parameters.vi"/>
				<Item Name="E9320 Video Averaging.vi" Type="VI" URL="../Public/Configure/E Series Sensors/E9320 Video Averaging.vi"/>
			</Item>
			<Item Name="Input" Type="Folder">
				<Item Name="Configure Averaging Auto.vi" Type="VI" URL="../Public/Configure/Input/Configure Averaging Auto.vi"/>
				<Item Name="Configure Averaging.vi" Type="VI" URL="../Public/Configure/Input/Configure Averaging.vi"/>
				<Item Name="Configure Channel Offset.vi" Type="VI" URL="../Public/Configure/Input/Configure Channel Offset.vi"/>
				<Item Name="Configure Input.vi" Type="VI" URL="../Public/Configure/Input/Configure Input.vi"/>
				<Item Name="Get Frequency Dependent Offset.vi" Type="VI" URL="../Public/Configure/Input/Get Frequency Dependent Offset.vi"/>

				<Item Name="Linearity Correction.vi" Type="VI" URL="../Public/Configure/Input/Linearity Correction.vi"/>
			</Item>
			<Item Name="Measurement" Type="Folder">
				<Item Name="Configure Measurement.vi" Type="VI" URL="../Public/Configure/Measurement/Configure Measurement.vi"/>
				<Item Name="Configure Average Trigger Measurement.vi" Type="VI" URL="../Public/Configure/Measurement/Configure Average Trigger Measurement.vi"/>
			</Item>
			<Item Name="Memory" Type="Folder">
				<Item Name="Calibration-Offset Tables" Type="Folder">

					<Item Name="Active Table Information.vi" Type="VI" URL="../Public/Configure/Memory/Calibration-Offset Tables/Active Table Information.vi"/>
					<Item Name="Active Table Values.vi" Type="VI" URL="../Public/Configure/Memory/Calibration-Offset Tables/Active Table Values.vi"/>
					<Item Name="Define Calibration Table.vi" Type="VI" URL="../Public/Configure/Memory/Calibration-Offset Tables/Define Calibration Table.vi"/>
					<Item Name="Define Offset Table.vi" Type="VI" URL="../Public/Configure/Memory/Calibration-Offset Tables/Define Offset Table.vi"/>
					<Item Name="Table Operation.vi" Type="VI" URL="../Public/Configure/Memory/Calibration-Offset Tables/Table Operation.vi"/>

				</Item>
				<Item Name="Save-Recall Register" Type="Folder">
					<Item Name="Load From Save Recall Register.vi" Type="VI" URL="../Public/Configure/Memory/Save-Recall Register/Load From Save Recall Register.vi"/>
					<Item Name="Register Count.vi" Type="VI" URL="../Public/Configure/Memory/Save-Recall Register/Register Count.vi"/>
					<Item Name="Register Map Name.vi" Type="VI" URL="../Public/Configure/Memory/Save-Recall Register/Register Map Name.vi"/>
					<Item Name="Save To Save Recall Register.vi" Type="VI" URL="../Public/Configure/Memory/Save-Recall Register/Save To Save Recall Register.vi"/>
				</Item>

				<Item Name="Memory Contents.vi" Type="VI" URL="../Public/Configure/Memory/Memory Contents.vi"/>
				<Item Name="Size Of Memory Block.vi" Type="VI" URL="../Public/Configure/Memory/Size Of Memory Block.vi"/>
			</Item>
			<Item Name="Non E Series Sensors" Type="Folder">
				<Item Name="U Series Sensors" Type="Folder">
					<Item Name="Configure Measurment Speed.vi" Type="VI" URL="../Public/Configure/Non E Series Sensors/U Series Sensors/Configure Measurment Speed.vi"/>
					<Item Name="U2000 Configure Calibration.vi" Type="VI" URL="../Public/Configure/Non E Series Sensors/U Series Sensors/U2000 Configure Calibration.vi"/>
				</Item>
				<Item Name="Configure Duty Cycle.vi" Type="VI" URL="../Public/Configure/Non E Series Sensors/Configure Duty Cycle.vi"/>
				<Item Name="Set Calibration Factor.vi" Type="VI" URL="../Public/Configure/Non E Series Sensors/Set Calibration Factor.vi"/>

				<Item Name="Set Corrections.vi" Type="VI" URL="../Public/Configure/Non E Series Sensors/Set Corrections.vi"/>

			</Item>
			<Item Name="Output" Type="Folder">
				<Item Name="Configure Recorder.vi" Type="VI" URL="../Public/Configure/Output/Configure Recorder.vi"/>
				<Item Name="Configure TTL.vi" Type="VI" URL="../Public/Configure/Output/Configure TTL.vi"/>
				<Item Name="E9320 Output Trigger State.vi" Type="VI" URL="../Public/Configure/Output/E9320 Output Trigger State.vi"/>
				<Item Name="Recorder Measurement.vi" Type="VI" URL="../Public/Configure/Output/Recorder Measurement.vi"/>
				<Item Name="Reference Oscillator State.vi" Type="VI" URL="../Public/Configure/Output/Reference Oscillator State.vi"/>

			</Item>
			<Item Name="Trigger" Type="Folder">
				<Item Name="Configure Channel A Trigger.vi" Type="VI" URL="../Public/Configure/Trigger/Configure Channel A Trigger.vi"/>
				<Item Name="Configure Trigger.vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger.vi"/>
				<Item Name="Configure Trigger N191X.vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger N191X.vi"/>
				<Item Name="Configure Trigger Hold.vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger Hold.vi"/>
				<Item Name="Set Initiate Continuous State.vi" Type="VI" URL="../Public/Configure/Trigger/Set Initiate Continuous State.vi"/>

				<Item Name="Trigger Event Count.vi" Type="VI" URL="../Public/Configure/Trigger/Trigger Event Count.vi"/>
			</Item>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="E Series Sensors" Type="Folder">
				<Item Name="E Series Measure.vi" Type="VI" URL="../Public/Data/E Series Sensors/E Series Measure.vi"/>
				<Item Name="E9320 Trace Data.vi" Type="VI" URL="../Public/Data/E Series Sensors/E9320 Trace Data.vi"/>

			</Item>
			<Item Name="Fetch.vi" Type="VI" URL="../Public/Data/Fetch.vi"/>
			<Item Name="Measure.vi" Type="VI" URL="../Public/Data/Measure.vi"/>

			<Item Name="Read.vi" Type="VI" URL="../Public/Data/Read.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">

			<Item Name="Error Message.vi" Type="VI" URL="../Public/Utility/Error Message.vi"/>
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Get Sensor Type.vi" Type="VI" URL="../Public/Utility/Get Sensor Type.vi"/>
			<Item Name="Instrument Type.vi" Type="VI" URL="../Public/Utility/Instrument Type.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>

			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Self Test.vi" Type="VI" URL="../Public/Utility/Self Test.vi"/>
			<Item Name="Utility Generate Instrument Error.vi" Type="VI" URL="../Public/Utility/Utility Generate Instrument Error.vi"/>
		</Item>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">

		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Utility Build Measure Command.vi" Type="VI" URL="../Private/Utility Build Measure Command.vi"/>
		<Item Name="Utility Default Instrument Setup.vi" Type="VI" URL="../Private/Utility Default Instrument Setup.vi"/>
		<Item Name="Utility Query Large Data BIN.vi" Type="VI" URL="../Private/Utility Query Large Data BIN.vi"/>
		<Item Name="Utility Read A Lot Of Bytes.vi" Type="VI" URL="../Private/Utility Read A Lot Of Bytes.vi"/>
		<Item Name="Utility Session Info.vi" Type="VI" URL="../Private/Utility Session Info.vi"/>
		<Item Name="Wait For OPC.vi" Type="VI" URL="../Private/Wait For OPC.vi"/>
	</Item>
	<Item Name="Controls" Type="Folder">
		<Item Name="Channel.ctl" Type="VI" URL="../Controls/Channel.ctl"/>
		<Item Name="Frequency.ctl" Type="VI" URL="../Controls/Frequency.ctl"/>
		<Item Name="Measurement.ctl" Type="VI" URL="../Controls/Measurement.ctl"/>
	</Item>
	<Item Name="Agilent E441X Series Readme.html" Type="Document" URL="../Agilent E441X Series Readme.html"/>
</Library>
